import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';

class Header extends Component {
    state = {  }
    render() { 
        return ( <div>
        <header>
            <h1><Fade bottom cascade>Gradients.</Fade></h1>
            <nav>
                <ul>  
                        <li><a href="mailto:umbertom.casa@gmail.com"><button>Contact</button></a></li>

                </ul>
            </nav>
        </header>
            <p className='header-title'>
            Handpicked amazing gradient <br/>
                for your real life projects ✌</p>
                
        </div> );
    }
}
 
export default Header;